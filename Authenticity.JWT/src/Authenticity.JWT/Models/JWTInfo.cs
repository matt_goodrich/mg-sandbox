﻿namespace Authenticity.JWT.Models
{
    public class JWTInfo
    {
        public string CookieName { get; set; }
        public string CookieValue { get; set; }
        public string JWTPayload { get; set; }
    }
}
