﻿using System.ComponentModel.DataAnnotations;

namespace Authenticity.JWT.Models
{
    public class ApplicationUser
    {
        public string UserName { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
