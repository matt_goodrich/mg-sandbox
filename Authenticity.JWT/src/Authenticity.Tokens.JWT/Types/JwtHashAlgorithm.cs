﻿namespace Authenticity.Tokens.JWT.Types
{
    public enum JwtHashAlgorithm
    {
        HS256,
        HS384,
        HS512
    }
}
