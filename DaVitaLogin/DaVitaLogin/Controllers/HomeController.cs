﻿using System.Web.Mvc;
using DaVitaLogin.Models;

namespace DaVitaLogin.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginForm model)
        {
            return View();
        }

        public ActionResult Help()
        {
            return View();
        }
    }
}