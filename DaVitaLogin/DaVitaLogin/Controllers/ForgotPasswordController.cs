﻿using System.Web.Mvc;
using DaVitaLogin.Models;

namespace DaVitaLogin.Controllers
{
    public class ForgotPasswordController : Controller
    {
        // GET: ForgotPassword
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(ForgotPasswordForm model)
        {
            return View();
        }

        public ActionResult SecurityQuestions()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SecurityQuestions(SecurityQuestionsForm model)
        {
            return View();
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ForgotPasswordChangeForm model)
        {
            return View();
        }
    }
}