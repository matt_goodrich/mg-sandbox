﻿using System.Web.Mvc;
using System.Web.Routing;

namespace DaVitaLogin
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Help",
                url: "Help",
                defaults: new { controller = "Home", action = "Help"}
            );

            routes.MapRoute(
                name: "ForgotUsername",
                url: "Forgot/Username",
                defaults: new { controller = "ForgotUsername", action = "Index" }
            );

            routes.MapRoute(
                name: "ForgotPassword",
                url: "Forgot/Password",
                defaults: new { controller = "ForgotPassword", action = "Index" }
            );

            routes.MapRoute(
                name: "ForgotPasswordSecurityQuestions",
                url: "Forgot/Password/{id}/SecurityQuestions",
                defaults: new { controller = "ForgotPassword", action = "SecurityQuestions", id= UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ForgotPasswordChangePassword",
                url: "Forgot/Password/{id}/ChangePassword",
                defaults: new { controller = "ForgotPassword", action = "ChangePassword", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ChangePassword",
                url: "Change/Password",
                defaults: new { controller = "ChangePassword", action = "Index" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
