﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DaVitaLogin.Models
{
    public class ForgotUsernameForm
    {
        [Required]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? DateOfBirth { get; set; }

        [Required]
        public int LastFourSSN { get; set; }
    }
}
