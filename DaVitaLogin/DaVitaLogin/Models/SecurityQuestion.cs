﻿using System.ComponentModel.DataAnnotations;

namespace DaVitaLogin.Models
{
    public class SecurityQuestion
    {
        public string Question { get; set; }

        [Required]
        public string Answer { get; set; }
    }
}
