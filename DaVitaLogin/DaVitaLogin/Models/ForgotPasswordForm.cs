﻿using System.ComponentModel.DataAnnotations;

namespace DaVitaLogin.Models
{
    public class ForgotPasswordForm
    {
        [Required]
        public string Username { get; set; }
    }
}
