﻿namespace DaVitaLogin.Models
{
    public class SecurityQuestionsForm
    {
        public SecurityQuestion Question1 { get; set; }
        public SecurityQuestion Question2 { get; set; }
        public SecurityQuestion Question3 { get; set; }
    }
}
