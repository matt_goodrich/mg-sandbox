﻿using System.ComponentModel.DataAnnotations;

namespace DaVitaLogin.Models
{
    public class ChangePasswordForm
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string CurrentPassword { get; set; }

        [Required]
        public string NewPassword { get; set; }

        [Required]
        public string ConfirmPassword { get; set; }
    }
}
