﻿using System.ComponentModel.DataAnnotations;

namespace DaVitaLogin.Models
{
    public class ForgotPasswordChangeForm
    {
        [Required]
        public string NewPassword { get; set; }

        [Required]
        public string ConfirmPassword { get; set; }
    }
}
