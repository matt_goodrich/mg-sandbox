﻿using System.ComponentModel.DataAnnotations;

namespace DaVitaLogin.Models
{
    public class LoginForm
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
