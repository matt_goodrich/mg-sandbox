package com.PEGRight.AccountLinking.Controllers;

import com.PEGRight.AccountLinking.Business.IUserMapping;
import com.PEGRight.AccountLinking.Models.LdapUser;
import com.PEGRight.AccountLinking.Models.UserData;
import com.PEGRight.AccountLinking.Validation.MappingFormValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sourceid.config.ConfigStore;
import org.sourceid.config.ConfigStoreFarm;
import org.sourceid.saml20.domain.IdpConnection;
import org.sourceid.saml20.domain.mgmt.ConnectionManager;
import org.sourceid.saml20.domain.mgmt.MgmtFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Controller
public class MapUserController {

    private final Log log = LogFactory.getLog(this.getClass());

    private static ConfigStore config;
    
    private IUserMapping userMapping;

    @Autowired
    MappingFormValidator mappingFormValidator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(mappingFormValidator);
        config = ConfigStoreFarm.getConfig("account-linking");
    }

    @Autowired
    public void setMappings(IUserMapping userMapping) {
        this.userMapping = userMapping;
    }
    
    @RequestMapping(value = "/Auth", method = RequestMethod.POST)
    public String doAuth(HttpServletRequest request) throws UnsupportedEncodingException {
    	String baseUrl = String.format("%s://%s:%d",request.getScheme(),  request.getServerName(), request.getServerPort());
    	String redirectUrl = String.format("%s/Account-Linking/MapUser?originalUrl=%s", baseUrl, URLEncoder.encode(request.getParameter("originalUrl"), "UTF-8"));
    	return "redirect:"+redirectUrl;
    }

    @RequestMapping(value = "/startSSO.ping", method = RequestMethod.GET)
    public String doProxy(HttpServletRequest request) throws UnsupportedEncodingException {
    	String baseUrl = String.format("%s://%s:%d",request.getScheme(),  request.getServerName(), request.getServerPort());
    	String originalUrl = String.format("%s/sp/startSSO.ping?PartnerIdpId=%s&TargetResource=%s", config.getStringValue("PingSsoBaseUrl", "https://NotConfigured:9031"), request.getParameter("PartnerIdpId"), request.getParameter("TargetResource"));
    	String targetResource = String.format("%s/Account-Linking/Auth?PartnerIdpId=%s&originalUrl=%s", baseUrl, request.getParameter("PartnerIdpId"), URLEncoder.encode(originalUrl, "UTF-8"));
        String redirectUrl = String.format("%s/sp/startSSO.ping?PartnerIdpId=%s&TargetResource=%s", config.getStringValue("PingSsoBaseUrl", "https://NotConfigured:9031"), request.getParameter("PartnerIdpId"), URLEncoder.encode(targetResource, "UTF-8"));
        return "redirect:"+redirectUrl;
    }

    @RequestMapping(value = "/MapUser", method = RequestMethod.POST)
    public String saveOrUpdateMapping(@ModelAttribute("mappingForm") @Validated LdapUser ldapUser, BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpServletRequest request) {
        log.debug("saveOrUpdateMapping() : " + ldapUser.toString());

        if (result.hasErrors()) {
        	LdapUser foundUser = findUser(request);
            populateDefaultModel(model, foundUser);
            return "mappings/mappingform";
        } else {
            if (ldapUser.getIsLinked().toUpperCase().equals("TRUE")) {
            	try {
            		userMapping.saveOrUpdate(ldapUser);
            	} 
            	catch (Exception e) {
            		log.error("Error Saving User Mapping", e);
            	}
            }

            String redirectUrl = request.getParameter("originalUrl");
            return "redirect:"+redirectUrl;
        }
    }

    @RequestMapping(value = "/MapUser", method = RequestMethod.GET)
    public String showMapUserForm(HttpServletRequest request, Model model) {

        log.debug("showMapUserForm()");

        LdapUser foundUser = findUser(request);

        if (foundUser != null) {
        	if (!foundUser.getIsLinked().toUpperCase().equals("TRUE")) {
		        populateDefaultModel(model, foundUser);
		
		        return "mappings/mappingform";
        	}
        }
        
        String redirectUrl = request.getParameter("originalUrl");
        return "redirect:"+redirectUrl;
    }

    private void populateDefaultModel(Model model, LdapUser ldapUser) {
    	if (ldapUser != null) {
	        model.addAttribute("mappingForm", ldapUser);
    	}
    }
    
    private LdapUser findUser(HttpServletRequest request) {
    	LdapUser foundUser = null;
        
        UserData userData = (UserData) request.getSession().getAttribute("LOGGEDIN_USER");
        if (userData.getAttributes().containsKey("EmailAddress")) {
        	foundUser = userMapping.findByEmail(userData.getAttributes().get("EmailAddress"));
        }
        if (foundUser == null) {		
	        if (userData.getAttributes().containsKey("FirstName") && userData.getAttributes().containsKey("LastName")) {
	        	foundUser = userMapping.findByFirstLastName(userData.getAttributes().get("FirstName"), userData.getAttributes().get("LastName"));
	        }
        }
        
        if (foundUser != null) {
        	if (userData.getAttributes().containsKey("FacebookId")) {
        		foundUser.setFacebookId(userData.getAttributes().get("FacebookId"));
        	}
        	
        	if (userData.getAttributes().containsKey("GoogleId")) {
        		foundUser.setGoogleId(userData.getAttributes().get("GoogleId"));
        	}
        	
        	if (userData.getAttributes().containsKey("TenantId")) {
        		foundUser.setTenantId(userData.getAttributes().get("TenantId"));
        	}
        	
        	if (userData.getAttributes().containsKey("TenantUserId")) {
        		foundUser.setTenantUserId(userData.getAttributes().get("TenantUserId"));
        	}
        }
        
        return foundUser;
    }
}
