package com.PEGRight.AccountLinking.Validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.PEGRight.AccountLinking.Business.IUserMapping;
import com.PEGRight.AccountLinking.Models.LdapUser;

/**
 * Created by Matthew Goodrich on 2/23/2016.
 */
@Component
public class MappingFormValidator implements Validator {

    @Autowired
    IUserMapping userMapping;

    @Override
    public boolean supports(Class<?> aClass) {
        return LdapUser.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

    	LdapUser user = (LdapUser) o;

        //ValidationUtils.rejectIfEmptyOrWhitespace(errors, "urlRegex", "NotEmpty.mappingForm.urlRegex");
        //ValidationUtils.rejectIfEmptyOrWhitespace(errors, "idPConnector", "NotEmpty.mappingForm.idPConnector");

        //if(mapping.getIdPConnector().equalsIgnoreCase("none")){
        //    errors.rejectValue("idPConnector", "NotEmpty.mappingForm.idPConnector");
        //}
    }
}
