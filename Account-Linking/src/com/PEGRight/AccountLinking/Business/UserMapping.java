package com.PEGRight.AccountLinking.Business;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sourceid.config.ConfigStore;
import org.sourceid.config.ConfigStoreFarm;
import org.sourceid.saml20.domain.AttributeSource;
import org.sourceid.saml20.domain.LdapDataSource;
import org.sourceid.saml20.domain.impl.LdapAttributeSource;
import org.sourceid.saml20.domain.mgmt.DataSourceManager;
import org.sourceid.saml20.domain.mgmt.MgmtFactory;
import org.sourceid.util.log.AttributeMap;
import org.springframework.stereotype.Service;

import com.PEGRight.AccountLinking.Models.LdapUser;
import com.pingidentity.common.util.ldap.ConnectionInfo;
import com.pingidentity.common.util.ldap.LDAPUtil;
import com.pingidentity.common.util.ldap.LDAPUtilOptions;
import com.pingidentity.sdk.provision.exception.NotFoundException;

@Service("usermapping")
public class UserMapping implements IUserMapping {

    private final Log log = LogFactory.getLog(this.getClass());

    private static ConfigStore config;

    private LDAPUtil ldapUtil;
    
    private DataSourceManager dataSourceManager;
    private LdapDataSource ldapDataSource;
    private ConnectionInfo connectionInfo;
    
    public UserMapping() {
        config = ConfigStoreFarm.getConfig("account-linking");
        init();
    }
    
    private void init() {
    	dataSourceManager = MgmtFactory.getDataSourceManager();
    	try
    	{
    		ldapDataSource = dataSourceManager.getLdapDataSource(config.getStringValue("Ldap-Name"));
    		log.debug("Connecting to LDAP: " + config.getStringValue("Ldap-Name"));
        	connectionInfo = ldapDataSource.getConnectionInfo();
    	
        	ldapUtil = LDAPUtil.getInstance(connectionInfo);
        } catch (Exception e) {
        	log.error("Caught exception when attempting to get instance of LDAP connection", e);
        }
    }

    @Override
    public LdapUser findByEmail(String emailAddress) {
    	return find("(mail=" + emailAddress + ")");
    }
    
    @Override
    public LdapUser findByFirstLastName(String firstName, String lastName) {
        return find("(&(givenName=" + firstName + ")(sn=" + lastName + "))");
    }

    private LdapUser find(String ldapSearchFilter) {
    	String searchFilter = ldapSearchFilter;//LDAPUtil.encodeFilter(ldapSearchFilter);
    	
    	AttributeMap searchResultsMap = null;
    	try {
            // log parameters to be queried
    		log.debug("FindUser - Querying LDAP with this searchbase: " + config.getStringValue("Ldap-Base-Dn"));
            log.debug("FindUser - Querying LDAP with this searchfilter: " + searchFilter);
            log.debug("FindUser - Querying LDAP with this searchscope: " + config.getStringValue("Ldap-Search-Scope"));

            LDAPUtilOptions ldapOptions = new LDAPUtilOptions(config.getStringValue("Ldap-Base-Dn"), searchFilter, config.getIntValue("Ldap-Search-Scope"));
            
            List lst = config.getListValue("Ldap-Attributes");
            
            String[] array = new String[lst.size()];
            int index = 0;
            for (Object value : lst) {
              array[index] = (String) value;
              index++;
            }
            
            ldapOptions.setAttributes(array);
            // perform LDAP query using getAttributesOfMatchingObjects method in LDAPUtil class
            // method returns AttributeMap if user found, else returns null AttributeMap
            // method throws Naming Exception
            searchResultsMap = ldapUtil.getAttributesOfMatchingObject(ldapOptions);

            if (!(searchResultsMap == null || searchResultsMap.isEmpty())) {
            	log.debug("FindUser - User exists in the LDAP");
            	
            	Set keys = searchResultsMap.keySet();
            	Iterator keyIterator = keys.iterator();

                while( keyIterator.hasNext( ) ) {
                    Object key = keyIterator.next( );
                    
                    String value = searchResultsMap.getSingleValue(key.toString());
                    
                	log.debug("FindUser: " + key.toString() + " : " + value);
                }
            	
            	LdapUser user = new LdapUser();
            	user.setCn(searchResultsMap.getSingleValue("cn"));
            	user.setDescription(searchResultsMap.getSingleValue("description"));
            	user.setDn(searchResultsMap.getSingleValue("Subject DN"));
            	user.setEmailAddress(searchResultsMap.getSingleValue("mail"));
            	user.setEmployeeNumber(searchResultsMap.getSingleValue("employeeNumber"));
            	user.setGivenName(searchResultsMap.getSingleValue("givenName"));
            	user.setHomePhone(searchResultsMap.getSingleValue("homePhone"));
            	user.setInitials(searchResultsMap.getSingleValue("initials"));
            	user.setLocality(searchResultsMap.getSingleValue("l"));
            	user.setMobilePhone(searchResultsMap.getSingleValue("mobile"));
            	user.setPager(searchResultsMap.getSingleValue("pager"));
            	user.setPostalAddress(searchResultsMap.getSingleValue("postalAddress"));
            	user.setPostalCode(searchResultsMap.getSingleValue("postalCode"));
            	user.setRegion(searchResultsMap.getSingleValue("st"));
            	user.setSn(searchResultsMap.getSingleValue("sn"));
            	user.setTelephoneNumber(searchResultsMap.getSingleValue("telephoneNumber"));
            	user.setUid(searchResultsMap.getSingleValue("uid"));
            	user.setIsLinked(searchResultsMap.getSingleValue("isLinked"));
            	user.setFacebookId(searchResultsMap.getSingleValue("facebookId"));
            	user.setGoogleId(searchResultsMap.getSingleValue("googleId"));
            	user.setTenantId(searchResultsMap.getSingleValue("tenantId"));
            	user.setTenantUserId(searchResultsMap.getSingleValue("tenantUserId"));
            	return user;
            	
            } else {
            	log.debug("FindUser - User does not exist in the LDAP");
                throw new NotFoundException("User not found from filter: " + searchFilter);
            }
        } catch (NamingException ex) {
            log.error("User not found from filter: " + searchFilter, ex);
        } catch (Exception ex) {
        	log.error("Error while querying LDAP", ex);
        }
    	return null;
    }
    
    @Override
    public void saveOrUpdate(LdapUser ldapUser) throws NamingException {
    	
    	LdapName currDn = new LdapName(ldapUser.getDn() == null ? "" : ldapUser.getDn());        
		LdapName currCn = new LdapName("");
		LdapName currOu = new LdapName("");
		for (Rdn rdn : currDn.getRdns()) {
			if (rdn.getType().equalsIgnoreCase("uid")) {
				currCn.add(rdn);
			} else {
				currOu.add(rdn);
			}
		}   
		
        log.debug("ModifyUser - user's current DN is : " + currDn);
        log.debug("ModifyUser - user's current OU is : " + currOu);
        log.debug("ModifyUser - user's current CN is : " + currCn);
    	
        AttributeSource attributeSource = new LdapAttributeSource();
        attributeSource.setDataSource(ldapDataSource);
        attributeSource.setDataSourceId(ldapDataSource.getId());
        attributeSource.setParameter("filter", currCn.toString());
        attributeSource.setParameter("search_base", currOu.toString());
        
        AttributeMap changeAttributeMap = new AttributeMap();
        changeAttributeMap.put("description", ldapUser.getDescription());
        changeAttributeMap.put("mail", ldapUser.getEmailAddress());
        changeAttributeMap.put("employeeNumber", ldapUser.getEmployeeNumber());
        changeAttributeMap.put("givenName", ldapUser.getGivenName());
        changeAttributeMap.put("homePhone", ldapUser.getHomePhone());
        changeAttributeMap.put("initials", ldapUser.getInitials());
        changeAttributeMap.put("l", ldapUser.getLocality());
        changeAttributeMap.put("mobile", ldapUser.getMobilePhone());
        changeAttributeMap.put("pager", ldapUser.getPager());
        changeAttributeMap.put("postalAddress", ldapUser.getPostalAddress());
        changeAttributeMap.put("postalCode", ldapUser.getPostalCode());
        changeAttributeMap.put("st", ldapUser.getRegion());
        changeAttributeMap.put("sn", ldapUser.getSn());
        changeAttributeMap.put("telephoneNumber", ldapUser.getTelephoneNumber());
        changeAttributeMap.put("isLinked", ldapUser.getIsLinked());
        changeAttributeMap.put("facebookId", ldapUser.getFacebookId());
        changeAttributeMap.put("googleId", ldapUser.getGoogleId());
        changeAttributeMap.put("tenantId", ldapUser.getTenantId());
        changeAttributeMap.put("tenantUserId", ldapUser.getTenantUserId());

        Attributes changeAttributes = LDAPUtil.loadAttrs(changeAttributeMap);

        ldapUtil.modifyUser(attributeSource, changeAttributes, new AttributeMap());
    }
}
