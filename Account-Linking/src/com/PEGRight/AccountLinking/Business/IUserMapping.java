package com.PEGRight.AccountLinking.Business;

import javax.naming.NamingException;

import com.PEGRight.AccountLinking.Models.LdapUser;

public interface IUserMapping {

    LdapUser findByEmail(String emailAddress);

    LdapUser findByFirstLastName(String firstName, String lastName);

    void saveOrUpdate(LdapUser ldapUser) throws NamingException;
}
