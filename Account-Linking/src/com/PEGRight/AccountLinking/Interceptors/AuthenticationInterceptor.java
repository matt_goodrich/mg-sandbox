package com.PEGRight.AccountLinking.Interceptors;

import com.PEGRight.AccountLinking.Models.UserData;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jose4j.json.internal.json_simple.JSONObject;
import org.jose4j.json.internal.json_simple.parser.JSONParser;
import org.sourceid.config.ConfigStore;
import org.sourceid.config.ConfigStoreFarm;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by Matthew Goodrich on 2/29/2016.
 */
public class AuthenticationInterceptor extends HandlerInterceptorAdapter {

    private final Log log = LogFactory.getLog(this.getClass());

    private static ConfigStore config;

    public AuthenticationInterceptor() {
        config = ConfigStoreFarm.getConfig("account-linking");
    }

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        log.info("Interceptor: Pre-handle");

        // Avoid a redirect loop for some urls
        //if (!request.getRequestURI().contains("/startSSO.ping")) {

            String refId = request.getParameter("REF");

            if (refId != null)
            {
                if (!refId.isEmpty())
                {
                    log.info("Found REF: " + refId);
                    // Use the trust manager to get an SSLSocketFactory
                    SSLContext sslContext = SSLContext.getInstance("TLS");
                    sslContext.init(null, null, null);
                    SSLSocketFactory socketFactory = sslContext.getSocketFactory();

                    // Call back to PF to get the attributes associated with the reference
                    String pickupLocation = String.format("%s/ext/ref/pickup?REF=%s",config.getStringValue("PingSsoBaseUrl", "https://NotConfigured:9031"), refId);
                    System.out.println(pickupLocation);
                    URL pickUrl = new URL(pickupLocation);
                    URLConnection urlConn = pickUrl.openConnection();
                    HttpsURLConnection httpsURLConn = (HttpsURLConnection)urlConn;
                    httpsURLConn.setSSLSocketFactory(socketFactory);
                    urlConn.setRequestProperty("ping.uname", config.getStringValue("PingUName", "NotConfigured"));
                    urlConn.setRequestProperty("ping.pwd", config.getStringValue("PingPwd", "NotConfigured"));
                    urlConn.setRequestProperty("ping.instanceId", config.getStringValue("PingInstanceId", "NotConfigured"));

                    acceptAllCertificates(urlConn);

                    // Get the response and parse it into another JSON object which are the
                    //'user attributes'.
                    // This example uses UTF-8 if encoding is not found in request.
                    String encoding = urlConn.getContentEncoding();
                    InputStream is = urlConn.getInputStream();
                    InputStreamReader streamReader = new InputStreamReader(is, encoding != null ? encoding : "UTF-8");


                    JSONParser parser = new JSONParser();
                    JSONObject spUserAttributes = (JSONObject)parser.parse(streamReader);
                    System.out.println("User Attributes received = " + spUserAttributes.toString());

                    UserData uData = new UserData();
                    uData.setUserId((String) spUserAttributes.get("username"));

                    request.getSession().setAttribute("LOGGEDIN_USER", uData);
                    
                    Map<String, String> attributes = new HashMap<String, String>();
                    
                    Set keySet = spUserAttributes.keySet( );
                    Iterator keyIterator = keySet.iterator();

                    while( keyIterator.hasNext( ) ) {
                        Object key = keyIterator.next( );
                        
                        String value = (String) spUserAttributes.get(key);
                        
                        if (value != null && !value.equals("") && !value.equals("None")) {
                        	log.debug("Adding Attribute to Session: " + key.toString() + " : " + value);
                        	attributes.put(key.toString(), value);
                        }
                    }
                    
                    uData.setAttributes(attributes);
                }
            }


            UserData userData = (UserData) request.getSession().getAttribute("LOGGEDIN_USER");
            if (userData == null) {
                String baseUrl = String.format("%s://%s:%d",request.getScheme(),  request.getServerName(), request.getServerPort());
                String originalUrl = String.format("%s/sp/startSSO.ping?PartnerIdpId=%s&TargetResource=%s", config.getStringValue("PingSsoBaseUrl", "https://NotConfigured:9031"), request.getParameter("PartnerIdpId"), request.getParameter("TargetResource"));
                String targetResource = String.format("%s/Account-Linking/Auth?PartnerIdpId=%s&originalUrl=%s", baseUrl, request.getParameter("PartnerIdpId"), URLEncoder.encode(originalUrl, "UTF-8"));
                String redirectUrl = String.format("%s/sp/startSSO.ping?PartnerIdpId=%s&TargetResource=%s", config.getStringValue("PingSsoBaseUrl", "https://NotConfigured:9031"), request.getParameter("PartnerIdpId"), URLEncoder.encode(targetResource, "UTF-8"));
                response.sendRedirect(redirectUrl);
                return false;
            }
        //}
        return true;
    }
    
    /** 
	 * Configures the URLConnection object to accept all HTTPS certificates, even if they don't
	 * "check-out" for one reason or another. This method allows for the programmer to use this
	 * reference code for testing without having to add the (likely self-signed) certificate to the
	 * CACERTS java store.
	 * <p>
	 * THIS SHOULD NOT BE USED IN A PRODUCTION ENVIRONMENT.
	 * 
	 * @param urlConnection a URLConnection object to be adjusted to trust all certificates.
	 * @throws Exception
	 */
	private void acceptAllCertificates(URLConnection urlConnection) throws Exception {
		SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(null, new TrustManager[] { new SimpleTrustManager() }, null);
		SSLSocketFactory socketFactory = sslContext.getSocketFactory();
		HttpsURLConnection httpsURLConnection = (HttpsURLConnection) urlConnection;
		httpsURLConnection.setSSLSocketFactory(socketFactory);
		httpsURLConnection.setHostnameVerifier(new SimpleHostnameVerifier());
	}
}
