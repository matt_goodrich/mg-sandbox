package com.PEGRight.AccountLinking.Models;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Matthew Goodrich on 2/29/2016.
 */
public class UserData {
	
	private String userId;
    private Map<String, String> attributes; 
	
	public UserData() {
		setAttributes(new HashMap<String, String>());
	}
	
    public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}
}
