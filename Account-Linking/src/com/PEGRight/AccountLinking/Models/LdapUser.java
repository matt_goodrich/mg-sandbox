package com.PEGRight.AccountLinking.Models;

public class LdapUser {

    public String dn;
    public String cn;
    public String sn;
    public String description;
    public String employeeNumber;
    public String givenName;
    public String homePhone;
    public String initials;
    public String locality;
    public String emailAddress;
    public String mobilePhone;
    public String pager;
    public String postalAddress;
    public String postalCode;
    public String region;
    public String telephoneNumber;
    public String uid;
    
    public String isLinked;
    public String facebookId;
    public String googleId;
    public String tenantId;
    public String tenantUserId;
    
    public LdapUser() {
		isLinked = "FALSE";
    }
    
    public String getDn() {
		return dn;
	}

	public void setDn(String dn) {
		this.dn = dn;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getInitials() {
		return initials;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getPager() {
		return pager;
	}

	public void setPager(String pager) {
		this.pager = pager;
	}

	public String getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getIsLinked() {
		return isLinked;
	}

	public void setIsLinked(String isLinked) {
		if (isLinked == null) {
			isLinked = "FALSE";
		}
		this.isLinked = isLinked;
	}
	
	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}
	
	public String getGoogleId() {
		return googleId;
	}

	public void setGoogleId(String googleId) {
		this.googleId = googleId;
	}
	
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	
	public String getTenantUserId() {
		return tenantUserId;
	}

	public void setTenantUserId(String tenantUserId) {
		this.tenantUserId = tenantUserId;
	}
	
	@Override
    public String toString() {
        return "LdapUser [dn=" + dn + 
        		", cn=" + cn + 
        		", sn=" + sn +
        		", description=" + description +
        		", employeeNumber=" + employeeNumber +
        		", givenName=" + givenName +
        		", homePhone=" + homePhone +
        		", initials=" + initials +
        		", locality=" + locality +
        		", emailAddress=" + emailAddress +
        		", mobilePhone=" + mobilePhone +
        		", pager=" + pager +
        		", postalAddress=" + postalAddress +
        		", postalCode=" + postalCode +
        		", region=" + region +
        		", telephoneNumber=" + telephoneNumber +
        		", uid=" + uid +
        		", isLinked=" + isLinked +
        		", facebookId=" + facebookId +
        		", googleId=" + googleId +
        		", tenantId=" + tenantId +
        		", tenantUserId=" + tenantUserId +
        		"]";
    }
}
