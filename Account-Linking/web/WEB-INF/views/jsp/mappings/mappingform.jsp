<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<body>

    <jsp:include page="../fragments/nav.jsp" />

    <div class="container">

        <tags:url value="/MapUser" var="mappingActionUrl" />

        <form:form class="form-horizontal" method="post" modelAttribute="mappingForm">

            <form:hidden path="dn" />
            <form:hidden path="cn" />
            <form:hidden path="uid" />
            <form:hidden path="description" />
            <form:hidden path="locality" />
            <form:hidden path="postalAddress" />
            <form:hidden path="postalCode" />
            <form:hidden path="region" />
            <form:hidden path="facebookId" />
            <form:hidden path="googleId" />
            <form:hidden path="tenantId" />
            <form:hidden path="tenantUserId" />

			<tags:bind path="givenName">
				<div class="form-group ${status.error ? 'has-error' : ''}">
	                <label class="col-sm-2 control-label">First Name</label>
	                <div class="col-sm-10">
	                    <form:input path="givenName" class="form-control " type="text" readonly="true" placeholder="First Name" />
	                    <form:errors path="givenName" class="control-label" />
	                </div>
	            </div>
            </tags:bind>
            
            <tags:bind path="sn">
	            <div class="form-group ${status.error ? 'has-error' : ''}">
	                <label class="col-sm-2 control-label">Last Name</label>
	                <div class="col-sm-10">
	                    <form:input path="sn" class="form-control " type="text" readonly="true" placeholder="Last Name" />
	                    <form:errors path="sn" class="control-label" />
	                </div>
	            </div>
            </tags:bind>
            
            <tags:bind path="initials">
	            <div class="form-group ${status.error ? 'has-error' : ''}">
	                <label class="col-sm-2 control-label">Initials</label>
	                <div class="col-sm-10">
	                    <form:input path="initials" class="form-control " type="text" readonly="true" placeholder="Initials" />
	                    <form:errors path="initials" class="control-label" />
	                </div>
	            </div>
			</tags:bind>
            
            <tags:bind path="employeeNumber">
	            <div class="form-group ${status.error ? 'has-error' : ''}">
	                <label class="col-sm-2 control-label">Employee Number</label>
	                <div class="col-sm-10">
	                    <form:input path="employeeNumber" class="form-control " type="text" readonly="true" placeholder="Employee Number" />
	                    <form:errors path="employeeNumber" class="control-label" />
	                </div>
	            </div>
			</tags:bind>
			
			<tags:bind path="homePhone">
	            <div class="form-group ${status.error ? 'has-error' : ''}">
	                <label class="col-sm-2 control-label">Home Phone</label>
	                <div class="col-sm-10">
	                    <form:input path="homePhone" class="form-control " type="text" readonly="true" placeholder="Home Phone Number" />
	                    <form:errors path="homePhone" class="control-label" />
	                </div>
	            </div>
			</tags:bind>
			
			<tags:bind path="mobilePhone">
	            <div class="form-group ${status.error ? 'has-error' : ''}">
	                <label class="col-sm-2 control-label">Mobile Phone</label>
	                <div class="col-sm-10">
	                    <form:input path="mobilePhone" class="form-control " type="text" readonly="true" placeholder="Mobile Phone Number" />
	                    <form:errors path="mobilePhone" class="control-label" />
	                </div>
	            </div>
			</tags:bind>
			
			<tags:bind path="pager">
	            <div class="form-group ${status.error ? 'has-error' : ''}">
	                <label class="col-sm-2 control-label">Pager</label>
	                <div class="col-sm-10">
	                    <form:input path="pager" class="form-control " type="text" readonly="true" placeholder="Pager Number" />
	                    <form:errors path="pager" class="control-label" />
	                </div>
	            </div>
			</tags:bind>
			
			<tags:bind path="telephoneNumber">
	            <div class="form-group ${status.error ? 'has-error' : ''}">
	                <label class="col-sm-2 control-label">Work Phone</label>
	                <div class="col-sm-10">
	                    <form:input path="telephoneNumber" class="form-control " type="text" readonly="true" placeholder="Work Phone Number" />
	                    <form:errors path="telephoneNumber" class="control-label" />
	                </div>
	            </div>
			</tags:bind>

			<tags:bind path="isLinked">
	            <div class="form-group ${status.error ? 'has-error' : ''}">
	                <label class="col-sm-2 control-label">Is This You?</label>
	                <div class="col-sm-10">
	                <div class="radio">
	                	<label>
	                    	<form:radiobutton path="isLinked" value="True" label="Yes" />
	                    </label>
                    </div>
                    <div class="radio">
                    	<label>
	                    	<form:radiobutton path="isLinked" value="False" label="No" />
                    	</label>
                    </div>
	                    <form:errors path="isLinked" class="control-label" />
	                </div>
	            </div>
            </tags:bind>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn-lg btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form:form>

    </div>

    <jsp:include page="../fragments/footer.jsp" />

</body>
</html>