<%@ page session="false"%>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="fragments/header.jsp" />

<body>

<div class="container">

    <h1>Oh No!</h1>

	<p>You likely ended up here because we cannot determine how to route your request.</p>

    <p>${exception.message}</p>
    <!-- Exception: ${exception.message}.
		  	<c:forEach items="${exception.stackTrace}" var="stackTrace">
				${stackTrace}
			</c:forEach>
	  	-->

</div>

<jsp:include page="fragments/footer.jsp" />

</body>
</html>