@echo off
REM /*************************************************************************
REM * 
REM * PEGRight CONFIDENTIAL
REM *
REM * __________________
REM * 
REM *  [2015] PEGRight, Inc. 
REM *  All Rights Reserved.
REM * 
REM * NOTICE:  All information contained herein is, and remains
REM * the property of PEGRight, Inc.  The intellectual and technical
REM * concepts contained herein are proprietary to PEGRight and may 
REM * be covered by U.S. and Foreign Patents, patents in process, 
REM * and are protected by trade secret or copyright law.
REM * Dissemination of this information or reproduction of this material
REM * is strictly forbidden unless prior written permission is obtained.
REM */


REM
REM Set environment variables for compile
REM
    echo .
    echo Setting environment for compile
    echo .
    set DEV_HOME=C:\WORK
    set JAVA_HOME=%DEV_HOME%\Java\JDK-1.8
    set ANT_HOME=%DEV_HOME%\Dev\ANT-1.9.2


REM
REM Verify ANT utility is present
REM
    echo .
    echo Searching for ANT at %ANT_HOME%
    echo .
    set STATUS=BAD
    if exist "%ANT_HOME%" set STATUS=OK
    if "%STATUS%ERROR"=="BADERROR" goto :error_ant
    set PATH=%PATH%;%ANT_HOME%\bin
    echo .
    echo ANT found at %ANT_HOME%
    echo .


REM
REM Verify JDK is present
REM
    echo .
    echo Searching for JAVA at %JAVA_HOME%
    echo .
    set STATUS=BAD
    if exist "%JAVA_HOME%" set STATUS=OK
    if "%STATUS%ERROR"=="BADERROR" goto :error_java
    set PATH=%PATH%;%JAVA_HOME%
    echo .
    echo JDK found at %JAVA_HOME%
    echo .


REM
REM Dump environment variables for troubleshooting
REM
    echo .
    echo Environment Dump
    echo .
    set


REM
REM Call ant targets to compile
REM
    echo .
    echo Starting Compile
    echo .
    echo on
    call ant -verbose -f build.xml clean-module
    call ant -verbose -f build.xml war-module
    goto :complete


REM
REM Error with Location of ANT
REM
:error_ant
    echo .
    echo ERROR - Unable to locate ANT utilities
    echo .
    goto :complete



REM
REM Error with Location of JAVA
REM
:error_java
    echo .
    echo ERROR - Unable to locate JAVA JDK
    echo .
    goto :complete



REM
REM Exit point in batch file
REM
:complete
    pause
