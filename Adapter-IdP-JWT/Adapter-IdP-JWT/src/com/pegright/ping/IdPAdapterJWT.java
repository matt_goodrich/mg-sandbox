package com.pegright.ping;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.HmacKey;
import org.json.JSONException;
import org.json.JSONObject;
import org.sourceid.saml20.adapter.AuthnAdapterException;
import org.sourceid.saml20.adapter.attribute.AttributeValue;
import org.sourceid.saml20.adapter.conf.Configuration;
import org.sourceid.saml20.adapter.gui.AdapterConfigurationGuiDescriptor;
import org.sourceid.saml20.adapter.idp.authn.AuthnPolicy;
import org.sourceid.saml20.adapter.idp.authn.IdpAuthnAdapterDescriptor;
import org.sourceid.util.log.AttributeMap;

import com.pegright.ping.util.AdapterSession;
import com.pegright.ping.util.AdapterSessionFactory;
import com.pingidentity.sdk.AuthnAdapterResponse;
import com.pingidentity.sdk.IdpAuthenticationAdapterV2;

public class IdPAdapterJWT implements IdpAuthenticationAdapterV2 {

	// Get reference to Ping server.log file
    private final Log log = LogFactory.getLog(this.getClass());
    
    // Define Labels for Admin GUI
    private static final String ADAPTER_VERSION = "1.0";
    private static final String COMPONENT_NAME = "JWT IdP Adapter";
    private static final String COMPONENT_DESC = "Adapter that will read a JWT from a cookie";
    
    private static final String CONFIG_AUTHN_SVC = "Authentication Service";
    private static final String CONFIG_AUTHN_SVC_DESC = "The URL of the Authentication Service used for Authentication";
    
    private static final String CONFIG_COOKIE_NAME = "Cookie Name";
    private static final String CONFIG_COOKIE_NAME_DESC = "The name of the cookie that contains the JWT";
    
    private static final String CONFIG_HMAC_KEY = "HMAC Key";
    private static final String CONFIG_HMAC_KEY_DESC = "The HMAC Key used to create the JWT";
    
    private static final String CONFIG_JWT_ISSUER = "JWT Issuer";
    private static final String CONFIG_JWT_ISSUER_DESC = "The Issuer of the JWT";
    
    private static final String CONFIG_JWT_AUDIENCE = "JWT Audience";
    private static final String CONFIG_JWT_AUDIENCE_DESC = "The Audience of the JWT";
    
    private static final String CONFIG_JWT_EXPIRATION_MINUTES = "JWT Expiration";
    private static final String CONFIG_JWT_EXPIRATION_MINUTES_DESC = "The expiration time in minutes";
    
    private static final String CONFIG_JWT_CLOCK_SKEW_SECONDS = "JWT Clock Skew";
    private static final String CONFIG_JWT_CLOCK_SKEW_SECONDS_DESC = "The allowable clock skew in seconds";
    
    private static final String ATTR_NAME_SUBJECT = "sub";
    
    private GuiHelper helper;
    
    // Define the UX interface and metadata descriptor for the Admin Console
    private IdpAuthnAdapterDescriptor descriptor;
    
    private String authnService;
    private String cookieName;
    private String hmacKey;
    private String jwtIssuer;
    private String jwtAudience;
    private int jwtExpiration;
    private int jwtClockSkew;
    
    public IdPAdapterJWT()
    {
    	this.descriptor = null;
    	this.helper = new GuiHelper();
    }
    
	@Override
	public IdpAuthnAdapterDescriptor getAdapterDescriptor() {
		if (this.descriptor == null)
        {
            this.descriptor = this.createAdapterDescriptor();
        }

        return (this.descriptor);
	}
	
	@Override
	public boolean logoutAuthN(Map authnIdentifiers, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, String resumePath) throws AuthnAdapterException, IOException {
		//Kill Cookie
		Cookie cookie = new Cookie(cookieName, "");
	    cookie.setMaxAge(0);
	    cookie.setPath("/");

	    httpServletResponse.addCookie(cookie);
	    return true;
	}
	
	@Override
	public void configure(Configuration configuration) {
		authnService = configuration.getFieldValue(CONFIG_AUTHN_SVC);
		cookieName = configuration.getFieldValue(CONFIG_COOKIE_NAME);
        hmacKey = configuration.getFieldValue(CONFIG_HMAC_KEY);
        jwtIssuer = configuration.getFieldValue(CONFIG_JWT_ISSUER);
        jwtAudience = configuration.getFieldValue(CONFIG_JWT_AUDIENCE);
        jwtExpiration = configuration.getIntFieldValue(CONFIG_JWT_EXPIRATION_MINUTES);
        jwtClockSkew = configuration.getIntFieldValue(CONFIG_JWT_CLOCK_SKEW_SECONDS);
	}
	
	@Override
	public Map<String, Object> getAdapterInfo() {
		return null;
	}
	
	@Override
	public AuthnAdapterResponse lookupAuthN(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Map<String, Object> inParameters) throws AuthnAdapterException, IOException {
		
		AdapterSession adapterSession = AdapterSessionFactory.getAdapterSession();
		
		AuthnAdapterResponse authnAdapterResponse = new AuthnAdapterResponse();
        authnAdapterResponse.setAuthnStatus(AuthnAdapterResponse.AUTHN_STATUS.FAILURE);
        
        Cookie[] cookies = httpServletRequest.getCookies();
        
        if (cookies != null)
        {
        	for(Cookie cookie : cookies)
        	{
        		log.debug("Cookie: " + cookie.getName());
        		if(cookieName.equals(cookie.getName()))
        		{
        			try
        			{
        				
        				
	        			JSONObject json = new JSONObject(URLDecoder.decode(cookie.getValue(), "UTF-8"));
	        			String jwt = json.getString("access_token");
	        			log.debug("JWT: " + jwt);
	        			// Use JwtConsumerBuilder to construct an appropriate JwtConsumer, which will
	        		    // be used to validate and process the JWT.
	        		    // The specific validation requirements for a JWT are context dependent, however,
	        		    // it typically advisable to require a (reasonable) expiration time, a trusted issuer, and
	        		    // and audience that identifies your system as the intended recipient.
	        		    // If the JWT is encrypted too, you need only provide a decryption key or
	        		    // decryption key resolver to the builder.
	        		    JwtConsumer jwtConsumer = new JwtConsumerBuilder()
	        		            .setRequireExpirationTime() // the JWT must have an expiration time
	        		            .setMaxFutureValidityInMinutes(jwtExpiration) // but the  expiration time can't be too crazy
	        		            .setAllowedClockSkewInSeconds(jwtClockSkew) // allow some leeway in validating time based claims to account for clock skew
	        		            .setRequireSubject() // the JWT must have a subject claim
	        		            .setExpectedIssuer(jwtIssuer) // whom the JWT needs to have been issued by
	        		            .setExpectedAudience(jwtAudience) // to whom the JWT is intended for
	        		            .setVerificationKey(new HmacKey(hmacKey.getBytes())) // verify the signature with the public key
	        		            .build(); // create the JwtConsumer instance

        		        //  Validate the JWT and process it to the Claims
        		        JwtClaims jwtClaims = jwtConsumer.processToClaims(jwt);
        		        log.debug("JWT validation succeeded! " + jwtClaims);
        		        
        		        Map authnIds = new AttributeMap();
            			authnIds.put(ATTR_NAME_SUBJECT, new AttributeValue(jwtClaims.getSubject()));
            			
            			for (String claim : jwtClaims.getClaimNames())
            			{
            				try
            				{
            					authnIds.put(claim, new AttributeValue(jwtClaims.getClaimValue(claim, String.class)));
            				}
            				catch (Exception e)
            				{
            					log.error("Claim value couldn't be converted to String: " + claim);
            				}
            			}
            			
            			authnAdapterResponse.setAttributeMap(authnIds);
                        authnAdapterResponse.setAuthnStatus(AuthnAdapterResponse.AUTHN_STATUS.SUCCESS);
                        
                        adapterSession.removeAttribute("NUMBER_OF_ATTEMPTS", httpServletRequest, httpServletResponse);
                        
                        return authnAdapterResponse;
        		        
        		    }
        		    catch (InvalidJwtException | MalformedClaimException | JSONException e)
        		    {
        		        // InvalidJwtException will be thrown, if the JWT failed processing or validation in anyway.
        		        // Hopefully with meaningful explanations(s) about what went wrong.
        		        log.error("Invalid JWT! " + e);
        		    }
        		}
        	}
        }
        
        AuthnPolicy authnPolicy = (AuthnPolicy) inParameters.get(IN_PARAMETER_NAME_AUTHN_POLICY);
        
        String strNumberOfAttempts = (String)adapterSession.getAttribute("NUMBER_OF_ATTEMPTS", httpServletRequest, httpServletResponse);
        int numberOfAttempts = 0;
        if (strNumberOfAttempts == null) {
          numberOfAttempts = 0;
        } else {
          numberOfAttempts = Integer.parseInt(strNumberOfAttempts);
        }
        numberOfAttempts++;
        
        adapterSession.setAttribute("NUMBER_OF_ATTEMPTS", Integer.toString(numberOfAttempts), httpServletRequest, httpServletResponse);
        if (numberOfAttempts > 1) {
          if ((inParameters.get(IN_PARAMETER_NAME_RESUME_PATH) != null) && (httpServletRequest.getRequestURI().endsWith(inParameters.get(IN_PARAMETER_NAME_RESUME_PATH).toString())))
          {
            adapterSession.removeAttribute("NUMBER_OF_ATTEMPTS", httpServletRequest, httpServletResponse);
            if (!authnPolicy.allowUserInteraction())
            {
            	return authnAdapterResponse;
            }
            throw new AuthnAdapterException("Could not obtain attributes from the IdP Authentication Service.");
          }
        }
        
        UrlHelper url = new UrlHelper(this.authnService);
        url.addParameter("resume", inParameters.get(IN_PARAMETER_NAME_RESUME_PATH).toString());
        url.addParameter("spentity", inParameters.get(IN_PARAMETER_NAME_PARTNER_ENTITYID).toString());
        if (!authnPolicy.allowUserInteraction()) {
          url.addParameter("IsPassive", "true");
        }
        if (authnPolicy.reauthenticate()) {
          url.addParameter("ForceAuthn", "true");
        }
        httpServletResponse.sendRedirect(url.toString());
        return null;
	}
	
	@Override
	public Map lookupAuthN(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, String partnerSpEntityId, AuthnPolicy authnPolicy, String resumePath) throws AuthnAdapterException, IOException {
		throw new UnsupportedOperationException();
	}
	
	private IdpAuthnAdapterDescriptor createAdapterDescriptor() {
		// Create an AdapterConfigurationGuiDescriptor that will tell PingFederate how to render
        // the configuration GUI screen and how to validate the user input
        final AdapterConfigurationGuiDescriptor adapterConfGuiDesc = new AdapterConfigurationGuiDescriptor();
        adapterConfGuiDesc.setDescription(COMPONENT_DESC);
        
        this.helper.addURLField(adapterConfGuiDesc, CONFIG_AUTHN_SVC, CONFIG_AUTHN_SVC_DESC, "", true, 25, false);
        this.helper.addTextField(adapterConfGuiDesc, CONFIG_COOKIE_NAME, CONFIG_COOKIE_NAME_DESC, "", true, false, 25, false);
        this.helper.addTextField(adapterConfGuiDesc, CONFIG_HMAC_KEY, CONFIG_HMAC_KEY_DESC, "", true, false, 25, false);
        this.helper.addTextField(adapterConfGuiDesc, CONFIG_JWT_ISSUER, CONFIG_JWT_ISSUER_DESC, "", true, false, 25, false);
        this.helper.addTextField(adapterConfGuiDesc, CONFIG_JWT_AUDIENCE, CONFIG_JWT_AUDIENCE_DESC, "", true, false, 25, false);
        this.helper.addIntField(adapterConfGuiDesc, CONFIG_JWT_EXPIRATION_MINUTES, CONFIG_JWT_EXPIRATION_MINUTES_DESC, "300", true, 0, 9999, false);
        this.helper.addIntField(adapterConfGuiDesc, CONFIG_JWT_CLOCK_SKEW_SECONDS, CONFIG_JWT_CLOCK_SKEW_SECONDS_DESC, "300", true, 0, 9999, false);
        
        Set<String> attrNames = new HashSet<String>();
        attrNames.add(ATTR_NAME_SUBJECT);
        
        return new IdpAuthnAdapterDescriptor(this,
                COMPONENT_NAME,
                attrNames,
                true,
                adapterConfGuiDesc,
                false,
                ADAPTER_VERSION );
	}
}

