package com.pegright.ping;

import java.net.MalformedURLException;
import java.net.URL;

public class UrlHelper
{
  private URL base;
  private StringBuffer paramBuf = new StringBuffer();
  
  public UrlHelper(String url)
    throws MalformedURLException
  {
    this.base = new URL(url);
  }
  
  public void addParameter(String name, String value)
  {
    if (this.paramBuf.length() > 0) {
      this.paramBuf.append('&');
    }
    this.paramBuf.append(name);
    this.paramBuf.append('=');
    this.paramBuf.append(value);
  }
  
  public String toString()
  {
    String query = this.base.getQuery();
    if (this.paramBuf.length() == 0) {
      return this.base.toExternalForm();
    }
    if (query == null)
    {
      if (this.paramBuf.indexOf("?") < 0) {
        this.paramBuf.insert(0, '?');
      }
    }
    else {
      this.paramBuf.insert(0, '&');
    }
    return this.base.toExternalForm() + this.paramBuf.toString();
  }
}
