package com.pegright.ping;

import com.pingidentity.sdk.GuiConfigDescriptor;
import org.sourceid.saml20.adapter.gui.*;
import org.sourceid.saml20.adapter.gui.validation.FieldValidator;
import org.sourceid.saml20.adapter.gui.validation.impl.HttpURLValidator;
import org.sourceid.saml20.adapter.gui.validation.impl.IntegerValidator;
import org.sourceid.saml20.adapter.gui.validation.impl.RequiredFieldValidator;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class GuiHelper {
    public TextFieldDescriptor addTextField(GuiConfigDescriptor gui, String key, String description, String defaultValue, boolean required, boolean encrypt, int len, boolean isAdvanced) {
        return addTextField(gui, key, description, defaultValue, required, encrypt, len, isAdvanced, null);
    }

    public TextFieldDescriptor addTextField(GuiConfigDescriptor gui, String key, String description, String defaultValue, boolean required, boolean encrypt, int len, boolean isAdvanced, List<FieldValidator> validators) {

        TextFieldDescriptor f = newTextField(key, description, encrypt, len);
        f.setDefaultValue(defaultValue);
        if (required) {
            f.addValidator(new RequiredFieldValidator());
        }

        if (validators != null) {
            for (FieldValidator validator : validators) {
                f.addValidator(validator);
            }
        }

        if (isAdvanced)
        {
            gui.addAdvancedField(f);
        }
        else {
            gui.addField(f);
        }

        return f;
    }

    public TextFieldDescriptor addIntField(GuiConfigDescriptor gui, String key, String description, String defaultValue, boolean required, int min, int max, boolean isAdvanced) {

        TextFieldDescriptor f = newTextField(key, description, false, 8);
        f.setDefaultValue(defaultValue);
        if(required) {
            f.addValidator(new RequiredFieldValidator());
        }

        f.addValidator(new IntegerValidator(min, max));

        if (isAdvanced)
        {
            gui.addAdvancedField(f);
        }
        else {
            gui.addField(f);
        }

        return f;
    }

    public RadioGroupFieldDescriptor addRadioGroupField(GuiConfigDescriptor gui, String key, String description, String defaultValue, String[] enumKeys, boolean required, boolean isAdvanced) {

        ArrayList options = new ArrayList();

        for(int f = 0; f < enumKeys.length; ++f) {
            options.add(new AbstractSelectionFieldDescriptor.OptionValue(enumKeys[f], Integer.toString(f)));
        }

        RadioGroupFieldDescriptor f = new RadioGroupFieldDescriptor(key, description, options);
        f.setDefaultValue(defaultValue);

        if(required) {
            f.addValidator(new RequiredFieldValidator());
        }

        if (isAdvanced)
        {
            gui.addAdvancedField(f);
        }
        else {
            gui.addField(f);
        }

        return f;
    }

    public SelectFieldDescriptor addDropdownField(GuiConfigDescriptor gui, String key, String description, String defaultValue, String[] enumKeys, boolean required, boolean isAdvanced) {

        ArrayList options = new ArrayList();

        for(int f = 0; f < enumKeys.length; ++f) {
            options.add(new AbstractSelectionFieldDescriptor.OptionValue(enumKeys[f], Integer.toString(f)));
        }

        SelectFieldDescriptor f = new SelectFieldDescriptor(key, description, options);
        f.setDefaultValue(defaultValue);

        if(required) {
            f.addValidator(new RequiredFieldValidator());
        }

        if (isAdvanced)
        {
            gui.addAdvancedField(f);
        }
        else {
            gui.addField(f);
        }

        return f;
    }

    public SelectFieldDescriptor addDropdownField(GuiConfigDescriptor gui, String key, String description, String defaultValue, ArrayList<AbstractSelectionFieldDescriptor.OptionValue> values, boolean required, boolean isAdvanced) {

        SelectFieldDescriptor f = new SelectFieldDescriptor(key, description, values);
        f.setDefaultValue(defaultValue);

        if(required) {
            f.addValidator(new RequiredFieldValidator());
        }

        if (isAdvanced)
        {
            gui.addAdvancedField(f);
        }
        else {
            gui.addField(f);
        }

        return f;
    }

    public CheckBoxFieldDescriptor addCheckboxField(GuiConfigDescriptor gui, String key, String description, boolean defaultChecked, boolean required, boolean isAdvanced) {

        CheckBoxFieldDescriptor f = new CheckBoxFieldDescriptor(key, description);
        f.setDefaultValue(defaultChecked);

        if (isAdvanced)
        {
            gui.addAdvancedField(f);
        }
        else {
            gui.addField(f);
        }

        return f;
    }

    public TextFieldDescriptor addURLField(GuiConfigDescriptor gui, String key, String description, String defaultValue, boolean required, int len, boolean isAdvanced) {

        TextFieldDescriptor f = newTextField(key, description, false, len);
        f.setDefaultValue(defaultValue);
        if(required) {
            f.addValidator(new RequiredFieldValidator());
        }

        f.addValidator(new HttpURLValidator());

        if (isAdvanced)
        {
            gui.addAdvancedField(f);
        }
        else {
            gui.addField(f);
        }

        return f;
    }

    private static final Class[] SET_SIZE_PARAM = { Integer.TYPE };

    public TextFieldDescriptor newTextField(String name, String desc, boolean encrypt, int length)
    {
        return newTextField(name, desc, encrypt, length, false, null);
    }

    public TextFieldDescriptor newTextField(String name, String desc, boolean encrypt, int length, boolean required, List<FieldValidator> validators)
    {
        TextFieldDescriptor tf = new TextFieldDescriptor(name, desc, encrypt);
        try
        {
            Method setLengthMethod = tf.getClass().getMethod("setSize", SET_SIZE_PARAM);
            if (setLengthMethod != null) {
                setLengthMethod.invoke(tf, new Object[] { Integer.valueOf(length) });
            }
        }
        catch (NoSuchMethodException e) {}catch (InvocationTargetException e)
        {
            throw new RuntimeException(e.getCause());
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(e.getCause());
        }

        if(required) {
            tf.addValidator(new RequiredFieldValidator());
        }

        if (validators != null) {
            for (FieldValidator validator : validators) {
                tf.addValidator(validator);
            }
        }

        return tf;
    }

    public SelectFieldDescriptor newDropdownField(String key, String description, ArrayList<AbstractSelectionFieldDescriptor.OptionValue> values, boolean required) {

        SelectFieldDescriptor f = new SelectFieldDescriptor(key, description, values);

        if(required) {
            f.addValidator(new RequiredFieldValidator());
        }

        return f;
    }
}