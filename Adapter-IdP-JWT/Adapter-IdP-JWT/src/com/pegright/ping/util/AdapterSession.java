package com.pegright.ping.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract interface AdapterSession
{
  public abstract void setAttribute(String paramString, Object paramObject, HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse);
  
  public abstract Object getAttribute(String paramString, HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse);
  
  public abstract Object removeAttribute(String paramString, HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse);
}
