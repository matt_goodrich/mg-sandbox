package com.pegright.ping.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AdapterHttpSession
  implements AdapterSession
{
  public Object getAttribute(String name, HttpServletRequest req, HttpServletResponse resp)
  {
    HttpSession session = req.getSession();
    return session.getAttribute(name);
  }
  
  public Object removeAttribute(String name, HttpServletRequest req, HttpServletResponse resp)
  {
    HttpSession session = req.getSession();
    
    Object value = session.getAttribute(name);
    session.removeAttribute(name);
    
    return value;
  }
  
  public void setAttribute(String name, Object value, HttpServletRequest req, HttpServletResponse resp)
  {
    HttpSession session = req.getSession();
    session.setAttribute(name, value);
  }
}
