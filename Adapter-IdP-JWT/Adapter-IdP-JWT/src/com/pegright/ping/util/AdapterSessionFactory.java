package com.pegright.ping.util;

public abstract class AdapterSessionFactory
{
  public static AdapterSession getAdapterSession()
  {
    AdapterSession adapterSession;
    try
    {
      adapterSession = new AdapterSessionStateSupport();
    }
    catch (NoClassDefFoundError e)
    {
      adapterSession = new AdapterHttpSession();
    }
    return adapterSession;
  }
}