﻿using System.Net;
using System.Text;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using PEGRight.SAML.Web.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace PEGRight.SAML.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index([FromForm] PostData data)
        {

            if (string.IsNullOrEmpty(data.SAMLResponse))
            {
                return Redirect("https://sso.ebsco-test.com:9031/idp/startSSO.ping?PartnerSpId=TestApp:SP");
            }

            var model = new SAMLInfo
            {
                SAMLResponse = !string.IsNullOrEmpty(data.SAMLResponse) ? data.SAMLResponse : "No SAMLResponse in Payload",
                SAMLDecode = !string.IsNullOrEmpty(data.SAMLResponse) ? Base64Decode(data.SAMLResponse) : "No SAMLResponse in Payload"
            };

            return View(model);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return WebUtility.HtmlEncode(Beautify(Encoding.UTF8.GetString(base64EncodedBytes))).Replace("\r\n", "<br />").Replace("  ", "&nbsp;&nbsp;");
        }

        public static string Beautify(string xml)
        {
            var doc = new XmlDocument();
            doc.LoadXml(xml);
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "  ",
                NewLineChars = "\r\n",
                NewLineHandling = NewLineHandling.Replace
            };
            using (XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                doc.Save(writer);
            }
            return sb.ToString();
        }
    }
}
