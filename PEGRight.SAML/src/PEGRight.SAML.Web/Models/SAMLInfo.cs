﻿namespace PEGRight.SAML.Web.Models
{
    public class SAMLInfo
    {
        public string SAMLResponse { get; set; }
        public string SAMLDecode { get; set; }
    }
}
