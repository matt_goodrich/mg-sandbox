package com.PEGRight.Idp.Discovery.Interceptors;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.X509TrustManager;

class SimpleTrustManager implements X509TrustManager {
	public SimpleTrustManager()	{	}

	@Override
	public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
		// does nothing - all certs are good.
	}


	@Override
	public X509Certificate[] getAcceptedIssuers() {
		// required method but not used in this reference implementation.
		return null;
	}


	@Override
	public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
		// checks validity of certificate - unused in this reference implementation.
		
	}
}
