package com.PEGRight.Idp.Discovery.Interceptors;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

/** 
 * This is only for use in test-cases where all certs should be trusted.
 * DO NOT USE IN PRODUCTION
 **/
public class SimpleHostnameVerifier implements HostnameVerifier {
	public boolean verify(String hostname, SSLSession session) {
		// always return true
		return true;
	}
}