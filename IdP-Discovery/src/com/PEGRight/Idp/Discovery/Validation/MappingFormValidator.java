package com.PEGRight.Idp.Discovery.Validation;

import com.PEGRight.Idp.Discovery.Business.IMappings;
import com.PEGRight.Idp.Discovery.Models.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Matthew Goodrich on 2/23/2016.
 */
@Component
public class MappingFormValidator implements Validator {

    @Autowired
    IMappings mappings;

    @Override
    public boolean supports(Class<?> aClass) {
        return Mapping.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        Mapping mapping = (Mapping) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "urlRegex", "NotEmpty.mappingForm.urlRegex");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "idPConnector", "NotEmpty.mappingForm.idPConnector");

        if(mapping.getIdPConnector().equalsIgnoreCase("none")){
            errors.rejectValue("idPConnector", "NotEmpty.mappingForm.idPConnector");
        }
    }
}
