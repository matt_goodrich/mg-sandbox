package com.PEGRight.Idp.Discovery.Business;

import com.PEGRight.Idp.Discovery.Models.Mapping;

import java.util.List;

public interface IMappings {

    Mapping findById(String id);

    Mapping findByUrl(String url);

    List<Mapping> getAll();

    void saveOrUpdate(Mapping mapping);

    void delete (String id);
}
