package com.PEGRight.Idp.Discovery.Business;

import com.PEGRight.Idp.Discovery.Models.Mapping;
import com.google.common.base.Strings;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sourceid.config.ConfigStore;
import org.sourceid.config.ConfigStoreFarm;
import org.sourceid.config.NoSuchValueException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service("mappings")
public class Mappings implements IMappings {

    private final Log log = LogFactory.getLog(this.getClass());

    private static ConfigStore config;

    public Mappings() {
        config = ConfigStoreFarm.getConfig("idp-mapping");
    }

    @Override
    public Mapping findById(String id) {
        String urlRegex = "";
        String idPConnector = "";

        try
        {
            urlRegex = config.getStringValue(id + "-UrlRegex");
            idPConnector = config.getStringValue(id + "-IdPConnector");
        }
        catch (Exception ex) {
            log.error("Error Extracting Config: " + id);
        }

        if (!Strings.nullToEmpty(urlRegex).trim().isEmpty() && !Strings.nullToEmpty(idPConnector).trim().isEmpty())
        {
            return new Mapping(id, urlRegex, idPConnector);
        }

        return null;
    }

    @Override
    public Mapping findByUrl(String url) {
        List<Mapping> allMappings = getAll();

        for (Mapping mapping : allMappings)
        {
            if (url.contains(mapping.getUrlRegex())) {
                return mapping;
            }
        }
        
        
        try {
        	Mapping defaultMapping = new Mapping();
			defaultMapping.setIdPConnector(config.getStringValue("DefaultIdPConnector"));
			return defaultMapping;
		} catch (NoSuchValueException e) {
			log.error("No Default IdP Connector Found", e);
		}
        
        return null;
    }

    @Override
    public List<Mapping> getAll() {

        List<String> configMappings = new ArrayList<>();

        try {
            configMappings = config.getListValue("mappings");
        }
        catch (Exception ex) {
            log.error("No Mappings Found in idp-mapping file", ex);
        }
        List<Mapping> mappings = new ArrayList<Mapping>();

        for(String mapping : configMappings)
        {
            String urlRegex = "";
            String idPConnector = "";

            try
            {
                urlRegex = config.getStringValue(mapping + "-UrlRegex");
                idPConnector = config.getStringValue(mapping + "-IdPConnector");
            }
            catch (Exception ex) {
                log.error("Error Extracting Config: " + mapping);
            }

            if (!Strings.nullToEmpty(urlRegex).trim().isEmpty() && !Strings.nullToEmpty(idPConnector).trim().isEmpty())
            {
                mappings.add(new Mapping(mapping, urlRegex, idPConnector));
            }
        }

        return mappings;
    }

    @Override
    public void saveOrUpdate(Mapping mapping) {
        if (findById(mapping.getId()) == null) {
            //Save
            UUID uuid = UUID.randomUUID();
            String randomUUIDString = uuid.toString();
            config.setStringValue(randomUUIDString + "-UrlRegex", mapping.getUrlRegex());
            config.setStringValue(randomUUIDString + "-IdPConnector", mapping.getIdPConnector());

            try {
                List<String> configMappings = config.getListValue("mappings");
                List<String> newConfigMappings = new ArrayList<>(configMappings);
                newConfigMappings.add(randomUUIDString);
                config.setListValue("mappings", newConfigMappings);
            }
            catch (Exception ex) {
                log.error("No Mappings Found in idp-mapping file", ex);
            }
        } else {
            //Update
            config.setStringValue(mapping.getId() + "-UrlRegex", mapping.getUrlRegex());
            config.setStringValue(mapping.getId() + "-IdPConnector", mapping.getIdPConnector());
        }
    }

    @Override
    public void delete(String id) {
        try {
            List<String> configMappings = config.getListValue("mappings");
            List<String> newConfigMappings = new ArrayList<>();
            for (String mapping : configMappings)
            {
                if (!mapping.equals(id)) {
                    newConfigMappings.add(mapping);
                }
            }
            config.setListValue("mappings", newConfigMappings);
        }
        catch (Exception ex) {
            log.error("No Mappings Found in idp-mapping file", ex);
        }
    }
}
