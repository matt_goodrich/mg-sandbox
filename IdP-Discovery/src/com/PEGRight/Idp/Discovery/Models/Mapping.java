package com.PEGRight.Idp.Discovery.Models;

public class Mapping {

    private String id;
    private String idPConnector;
    private String urlRegex;

    public Mapping() {

    }

    public Mapping(String urlRegex, String idPConnector) {
        this();
        this.urlRegex = urlRegex;
        this.idPConnector = idPConnector;
    }

    public Mapping(String id, String urlRegex, String idPConnector) {
        this(urlRegex, idPConnector);
        this.id = id;
    }

    public boolean isNew() {
        return (this.id == null);
    }

    public String getId() {return id;}

    public String getIdPConnector() {
        return idPConnector;
    }

    public String getUrlRegex() {
        return urlRegex;
    }

    public void setId(String id) { this.id = id; }

    public void setIdPConnector(String idPConnector) {
        this.idPConnector = idPConnector;
    }

    public void setUrlRegex(String urlRegex) {
        this.urlRegex = urlRegex;
    }

    @Override
    public String toString() {
        return "Mapping [id=" + id + ", IdPConnector=" + idPConnector + ", UrlRegex=" + urlRegex + "]" + isNew();
    }
}
