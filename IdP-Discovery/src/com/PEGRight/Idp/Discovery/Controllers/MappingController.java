package com.PEGRight.Idp.Discovery.Controllers;

import com.PEGRight.Idp.Discovery.Business.IMappings;
import com.PEGRight.Idp.Discovery.Models.Mapping;
import com.PEGRight.Idp.Discovery.Validation.MappingFormValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sourceid.saml20.domain.IdpConnection;
import org.sourceid.saml20.domain.mgmt.ConnectionManager;
import org.sourceid.saml20.domain.mgmt.MgmtFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Controller
public class MappingController {

    private final Log log = LogFactory.getLog(this.getClass());

    private IMappings mappings;

    @Autowired
    MappingFormValidator mappingFormValidator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(mappingFormValidator);
    }

    @Autowired
    public void setMappings(IMappings mappings) {
        this.mappings = mappings;
    }

    @RequestMapping(value = "/")
    public String index(Model model) {
        log.debug("index()");
        return "redirect:/mappings";
    }

    @RequestMapping(value = "/startSSO.ping", method = RequestMethod.GET)
    public String doProxy(HttpServletRequest request) {
        String targetResource = request.getParameter("TargetResource");
        Mapping foundMapping = mappings.findByUrl(targetResource);
        if (foundMapping != null) {
            String baseUrl = String.format("%s://%s:%d/Account-Linking/startSSO.ping?PartnerIdpId=%s&TargetResource=%s",request.getScheme(),  request.getServerName(), request.getServerPort(), foundMapping.getIdPConnector(), targetResource);
            return "redirect:"+baseUrl;
        }
        return null;
    }

    @RequestMapping(value = "/mappings", method = RequestMethod.GET)
    public String showAllMappings(Model model) {
        log.debug("showAllMappings()");
        model.addAttribute("mappings", mappings.getAll());
        return "mappings/list";
    }

    @RequestMapping(value = "/mappings", method = RequestMethod.POST)
    public String saveOrUpdateMapping(@ModelAttribute("mappingForm") @Validated Mapping mapping, BindingResult result, Model model, final RedirectAttributes redirectAttributes) {
        log.debug("saveOrUpdateMapping() : " + mapping.toString());

        if (result.hasErrors()) {
            populateDefaultModel(model);
            return "mappings/mappingform";
        } else {
            redirectAttributes.addFlashAttribute("css", "success");
            if (mapping.isNew()) {
                redirectAttributes.addFlashAttribute("msg", "Mapping added successfully!");
            }else{
                redirectAttributes.addFlashAttribute("msg", "Mapping updated successfully!");
            }

            mappings.saveOrUpdate(mapping);

            return "redirect:/mappings";
        }
    }

    @RequestMapping(value = "/mappings/add", method = RequestMethod.GET)
    public String showAddMappingForm(Model model) {

        log.debug("showAddMappingForm()");

        Mapping mapping = new Mapping();

        // set default value
        mapping.setIdPConnector("none");
        mapping.setUrlRegex("");

        model.addAttribute("mappingForm", mapping);

        populateDefaultModel(model);

        return "mappings/mappingform";
    }

    @RequestMapping(value = "/mappings/{id}/update", method = RequestMethod.GET)
    public String showUpdateMappingForm(@PathVariable("id") String id, Model model) {

        log.debug("showUpdateMappingForm() : " + id);

        Mapping mapping = mappings.findById(id);
        model.addAttribute("mappingForm", mapping);

        populateDefaultModel(model);

        return "mappings/mappingform";

    }

    @RequestMapping(value = "/mappings/{id}/delete", method = RequestMethod.GET)
    public String deleteMapping(@PathVariable("id") String id, final RedirectAttributes redirectAttributes) {

        log.debug("deleteMapping() : " + id);

        mappings.delete(id);

        redirectAttributes.addFlashAttribute("css", "success");
        redirectAttributes.addFlashAttribute("msg", "Mapping is deleted!");

        return "redirect:/mappings";

    }

    private void populateDefaultModel(Model model) {

        ConnectionManager connMgr = MgmtFactory.getConnectionManager();
        Collection<IdpConnection> idPConnections = connMgr.getAllIdpConnections();

        Map<String, String> idPCons = new LinkedHashMap<String, String>();

        for(IdpConnection con : idPConnections)
        {
            idPCons.put(con.getEntityId(), con.getName());
        }

        model.addAttribute("idPConnectorList", idPCons);
    }
}
