<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<body>

    <jsp:include page="../fragments/nav.jsp" />

    <div class="container">

        <core:choose>
            <core:when test="${mappingForm['new']}">
                <h1>Add Mapping</h1>
            </core:when>
            <core:otherwise>
                <h1>Update Mapping</h1>
            </core:otherwise>
        </core:choose>

        <tags:url value="/mappings" var="mappingActionUrl" />

        <form:form class="form-horizontal" method="post" modelAttribute="mappingForm" action="${mappingActionUrl}">

            <form:hidden path="id" />

            <tags:bind path="urlRegex">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Target Resource URL</label>
                    <div class="col-sm-10">
                        <form:input path="urlRegex" type="text" class="form-control " id="name" placeholder="URL" />
                        <form:errors path="urlRegex" class="control-label" />
                    </div>
                </div>
            </tags:bind>

            <tags:bind path="idPConnector">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">IdP Connector</label>
                    <div class="col-sm-5">
                        <form:select path="idPConnector" class="form-control">
                            <form:option value="NONE" label="--- Select ---" />
                            <form:options items="${idPConnectorList}" />
                        </form:select>
                        <form:errors path="idPConnector" class="control-label" />
                    </div>
                    <div class="col-sm-5"></div>
                </div>
            </tags:bind>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <core:choose>
                        <core:when test="${mappingForm['new']}">
                            <button type="submit" class="btn-lg btn-primary pull-right">Add</button>
                        </core:when>
                        <core:otherwise>
                            <button type="submit" class="btn-lg btn-primary pull-right">Update</button>
                        </core:otherwise>
                    </core:choose>
                </div>
            </div>
        </form:form>

    </div>

    <jsp:include page="../fragments/footer.jsp" />

</body>
</html>