<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<body>
    <jsp:include page="../fragments/nav.jsp" />

    <div class="container">

        <core:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </core:if>

        <h1>All Mappings</h1>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>Target Resource</th>
                <th>IdP Connector</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <core:forEach items="${mappings}" var="mapping">
                <tr>
                    <td>${mapping.urlRegex}</td>
                    <td>${mapping.idPConnector}</td>
                    <td>
                        <tags:url value="/mappings/${mapping.id}/delete" var="deleteUrl" />
                        <tags:url value="/mappings/${mapping.id}/update" var="updateUrl" />

                        <button class="btn btn-primary" onclick="location.href='${updateUrl}'">Update</button>
                        <button class="btn btn-danger" onclick="this.disabled=true;location.href='${deleteUrl}'">Delete</button>
                    </td>
                </tr>
            </core:forEach>
            </tbody>
        </table>

    </div>

    <jsp:include page="../fragments/footer.jsp" />

</body>
</html>