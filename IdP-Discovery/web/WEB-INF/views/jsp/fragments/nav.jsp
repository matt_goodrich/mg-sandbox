<%@ taglib prefix="tags" uri="http://www.springframework.org/tags"%>

<tags:url value="/" var="urlHome" />
<tags:url value="/mappings/add" var="urlAddMapping" />

<nav class="navbar navbar-inverse ">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="${urlHome}">IdP Discovery Mappings</a>
        </div>
        <div id="navbar">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="${urlAddMapping}">Add Mapping</a></li>
            </ul>
        </div>
    </div>
</nav>