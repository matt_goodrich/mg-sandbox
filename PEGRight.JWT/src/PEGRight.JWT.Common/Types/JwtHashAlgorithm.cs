﻿namespace PEGRight.JWT.Common.Types
{
    public enum JwtHashAlgorithm
    {
        HS256,
        HS384,
        HS512
    }
}
