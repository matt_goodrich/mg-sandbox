﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PEGRight.JWT.Web.Models;
using PEGRight.JWT.Common;
using PEGRight.JWT.Common.Options;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Authenticity.JWT.Controllers
{
    public class HomeController : Controller
    {
        private readonly HttpContext _context;

        private readonly JwtIssuerOptions _jwtOptions;
        private readonly ILogger _logger;
        private readonly JsonSerializerSettings _serializerSettings;

        public HomeController(IOptions<JwtIssuerOptions> jwtOptions, ILoggerFactory loggerFactory, IHttpContextAccessor httpContextAccessor)
        {
            _jwtOptions = jwtOptions.Value;
            TokenValidation.ThrowIfInvalidOptions(_jwtOptions);

            _logger = loggerFactory.CreateLogger<HomeController>();

            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };

            _context = httpContextAccessor.HttpContext;
        }

        [HttpGet]
        // GET: /<controller>/
        public IActionResult Index()
        {
            var model = new ApplicationUser();

            return View(model);
        }

        [HttpPost]
        public IActionResult Index([FromForm] ApplicationUser applicationUser)
        {
            var identity = GetClaimsIdentity(applicationUser);
            if (identity == null)
            {
                _logger.LogInformation($"Invalid username ({applicationUser.UserName}) or password ({applicationUser.Password})");
                ModelState.AddModelError("", "Invalid credentials");
                return View("Index", applicationUser);
            }

            var encodedJwt = PEGRight.JWT.Common.JWT.Encode(_jwtOptions, applicationUser.UserName, new List<Claim> { identity.FindFirst("CustomerGroup")});

            // Serialize and return the response
            var response = new
            {
                access_token = encodedJwt,
                expires_in = (int)_jwtOptions.ValidFor.TotalSeconds
            };

            var json = JsonConvert.SerializeObject(response, _serializerSettings);

            var cookieOptions = new CookieOptions
            {
                Domain = "localhost",
                Secure = false,//Should be true in production
                Path = "/"
            };
            
            Response.Cookies.Append("PEGRIGHT-JWT", json, cookieOptions);

            if (_context.Request.Query.ContainsKey("resume"))
            {
                var resume = _context.Request.Query.FirstOrDefault(x => x.Key == "resume").Value;
                return Redirect("https://sso.pegright-test:9031" + resume);
            }

            return RedirectToAction("Index", "Validate");
        }

        /// <summary>
        /// IMAGINE BIG RED WARNING SIGNS HERE!
        /// You'd want to retrieve claims through your claims provider
        /// in whatever way suits you, the below is purely for demo purposes!
        /// </summary>
        private static ClaimsIdentity GetClaimsIdentity(ApplicationUser user)
        {
            if (user.UserName == "TestUser" &&
                user.Password == "TestUser123")
            {
                return new ClaimsIdentity(new GenericIdentity(user.UserName, "Token"),
                    new[]
                    {
                        new Claim("CustomerGroup", "EBSCO1234")
                    });
            }

            // Credentials are invalid, or account doesn't exist
            return null;
        }
    }
}

