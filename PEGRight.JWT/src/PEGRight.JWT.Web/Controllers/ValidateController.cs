﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PEGRight.JWT.Web.Models;
using PEGRight.JWT.Common;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace PEGRight.JWT.Web.Controllers
{
    public class ValidateController : Controller
    {
        private readonly ILogger _logger;
        private readonly JsonSerializerSettings _serializerSettings;

        public ValidateController(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<ValidateController>();

            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            var dict = JsonConvert.DeserializeObject<Dictionary<string,object>>(Request.Cookies["EBSCO-JWT"]);
            var model = new JWTInfo
            {
                CookieName = "EBSCO-JWT",
                CookieValue = Request.Cookies["EBSCO-JWT"],
                JWTPayload = PEGRight.JWT.Common.JWT.Decode(dict["access_token"].ToString(), "586ea3bb55f6d8874346402c277be1b51bd77e1f94e469cc849a00a4c1a828cc")
            };
            return View(model);
        }
    }
}
