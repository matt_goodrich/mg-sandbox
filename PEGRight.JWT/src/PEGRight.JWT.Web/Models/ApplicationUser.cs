﻿using System.ComponentModel.DataAnnotations;

namespace PEGRight.JWT.Web.Models
{
    public class ApplicationUser
    {
        public string UserName { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
