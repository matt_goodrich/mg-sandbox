﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using static System.Int32;

namespace SBD.Decrypt
{
    class Program
    {
        static void Main(string[] args)
        {
            var originalPassword = "password";
            //This one was generated in Java
            //var generatedSecuredPasswordHash = "1000:3ea9469d7e578159dbacc3ff69f178ce:ccc7b11021687ae2250faabd0a710352fd5e8f874b0f52a9028dbbbdc1440d73e6cd9857fa886bd99d11a913a99df421a0423e80406c1d5667d275b9497a4e92";
            //This one was generated in C#
            var generatedSecuredPasswordHash = "1000:b3971b0a94d536d5053d75da6a7cbf7e:fe61ea9003618aa092b554780f654c1498862c0cf5397379aed251b58dbb889285f4be0eb7148383508c9296a3042adf37f0b8b5bbd6ba00412dcb43d45d24ca";
            //var generatedSecuredPasswordHash = GenerateStorngPasswordHash(originalPassword);
            Console.WriteLine(generatedSecuredPasswordHash);

            var result = ValidatePassword("password", generatedSecuredPasswordHash);
            Console.WriteLine(result);

            result = ValidatePassword("password1", generatedSecuredPasswordHash);
            Console.WriteLine(result);

            Console.ReadLine();
        }

        private static string GenerateStorngPasswordHash(String password)
        {
            var iterations = 1000;
            var salt = GetSalt();

            var spec = new Rfc2898DeriveBytes(password, salt, iterations);
            byte[] hash = spec.GetBytes(64);
            return iterations + ":" + ToHex(salt) + ":" + ToHex(hash);
        }

        private static byte[] GetSalt()
        {
            var sr = new RNGCryptoServiceProvider();
            var salt = new byte[16];
            sr.GetBytes(salt);
            return salt;
        }

        private static string ToHex(byte[] array)
        {
            var hex = new StringBuilder(array.Length * 2);
            foreach (byte b in array)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        private static bool ValidatePassword(string password, string storedPassword)
        {
            var parts = storedPassword.Split(':');
            var iterations = Parse(parts[0]);
            byte[] salt = FromHex(parts[1]);
            byte[] hash = FromHex(parts[2]);

            var spec = new Rfc2898DeriveBytes(password, salt, iterations);
            var testHash = spec.GetBytes(hash.Length);

            var diff = hash.Length ^ testHash.Length;
            for (var i = 0; i < hash.Length && i < testHash.Length; i++)
            {
                diff |= hash[i] ^ testHash[i];
            }

            return diff == 0;
        }

        private static byte[] FromHex(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                     .Where(x => x % 2 == 0)
                     .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                     .ToArray();
        }
    }
}
