package com.pegright.tokens.generator.jwt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.jose4j.lang.JoseException;

import com.typesafe.config.ConfigFactory;

public class App {

	public static void main(String[] args) {
			
		TokenInfo info = initializeTokenInfo();
		
		JWTTokenGenerator generator = new JWTTokenGenerator();
		
		info.setSubject("matt.goodrich@pegright.com");
		
		try {
			String token = generator.generateToken(info);
			System.out.println(token);
			sendToSalesForce(token);
		} catch (JoseException | UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void sendToSalesForce(String token) throws IOException {
		URL url = new URL("https://login.salesforce.com/services/oauth2/token");
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("grant_type", "urn:ietf:params:oauth:grant-type:jwt-bearer");
        params.put("assertion", token);

        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");

        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);

        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

        for (int c; (c = in.read()) >= 0;)
            System.out.print((char)c);
	}
	
	public static TokenInfo initializeTokenInfo() {
		TokenInfo tokenInfo = new TokenInfo();
		
        com.typesafe.config.Config config = ConfigFactory.load();
        
        tokenInfo.setIssuer(config.getString( "jose.issuer" ));
        tokenInfo.setAudience(config.getString( "jose.audience" ));
        tokenInfo.setExpirationMinutes(config.getInt( "jose.expirationMinutes" ));
        tokenInfo.setNotBeforeMinutes(config.getInt( "jose.notBeforeMinutes" ));
        tokenInfo.setKeyId(config.getString( "jose.keyId" ));
        tokenInfo.setKeyFileName(config.getString( "jose.keyFileName" ));
        tokenInfo.setKeyFilePassword(config.getString( "jose.keyFilePassword" ));
        tokenInfo.setKeyAlias(config.getString( "jose.keyAlias" ));
        
        return tokenInfo;
    }
}
