package com.pegright.tokens.generator.jwt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TokenInfo {
	
	private String issuer;
	private String audience;
	private int expirationMinutes;
	private int notBeforeMinutes;
	private String subject;
	private String keyId;
	
	private Map<String, Object> claims = new HashMap<String,Object>();
	private Map<String, List<String>> multiValueClaims = new HashMap<String, List<String>>();
	
	private String keyFileName;
	private String keyFilePassword;
	private String keyAlias;

	public String getIssuer() {
		return issuer;
	}
	
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	
	public String getAudience() {
		return audience;
	}
	
	public void setAudience(String audience) {
		this.audience = audience;
	}
	
	public int getExpirationMinutes() {
		return expirationMinutes;
	}
	
	public void setExpirationMinutes(int expirationMinutes) {
		this.expirationMinutes = expirationMinutes;
	}
	
	public int getNotBeforeMinutes() {
		return notBeforeMinutes;
	}
	
	public void setNotBeforeMinutes(int notBeforeMinutes) {
		this.notBeforeMinutes = notBeforeMinutes;
	}
	
	public String getSubject() {
		return subject;
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String getKeyId() {
		return keyId;
	}

	public void setKeyId(String keyId) {
		this.keyId = keyId;
	}
	
	public Map<String, Object> getClaims() {
		return claims;
	}

	public void setClaims(Map<String, Object> claims) {
		this.claims = claims;
	}

	public Map<String, List<String>> getMultiValueClaims() {
		return multiValueClaims;
	}

	public void setMultiValueClaims(Map<String, List<String>> multiValueClaims) {
		this.multiValueClaims = multiValueClaims;
	}

	public String getKeyFileName() {
		return keyFileName;
	}

	public void setKeyFileName(String keyFileName) {
		this.keyFileName = keyFileName;
	}

	public String getKeyFilePassword() {
		return keyFilePassword;
	}

	public void setKeyFilePassword(String keyFilePassword) {
		this.keyFilePassword = keyFilePassword;
	}

	public String getKeyAlias() {
		return keyAlias;
	}

	public void setKeyAlias(String keyAlias) {
		this.keyAlias = keyAlias;
	}
}
