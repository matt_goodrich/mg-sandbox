package com.pegright.tokens.generator.jwt;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.security.auth.x500.X500PrivateCredential;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.lang.JoseException;

public class JWTTokenGenerator {

	private final Log log = LogFactory.getLog(this.getClass());
	
	public JWTTokenGenerator()
	{
		
	}

	public String generateToken(TokenInfo tokenInfo) throws JoseException, KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, UnrecoverableKeyException {
		JwtClaims claims = new JwtClaims();
	    claims.setIssuer(tokenInfo.getIssuer());  // who creates the token and signs it
	    claims.setAudience(tokenInfo.getAudience()); // to whom the token is intended to be sent
	    claims.setExpirationTimeMinutesInTheFuture(tokenInfo.getExpirationMinutes()); // time when the token will expire (10 minutes from now)
	    claims.setGeneratedJwtId(); // a unique identifier for the token
	    claims.setIssuedAtToNow();  // when the token was issued/created (now)
	    claims.setNotBeforeMinutesInThePast(tokenInfo.getNotBeforeMinutes()); // time before which the token is not yet valid (2 minutes ago)
	    claims.setSubject(tokenInfo.getSubject()); // the subject/principal is whom the token is about
	    // additional claims/attributes about the subject can be added
	    for(Map.Entry<String, Object> entry: tokenInfo.getClaims().entrySet())
	    {
	    	claims.setClaim(entry.getKey(), entry.getValue());
	    }
	    // multi-valued claims work too and will end up as a JSON array
	    for(Entry<String, List<String>> entry: tokenInfo.getMultiValueClaims().entrySet())
	    {
	    	claims.setStringListClaim(entry.getKey(), entry.getValue());
	    }
	    
	    // A JWT is a JWS and/or a JWE with JSON claims as the payload.
	    // In this example it is a JWS so we create a JsonWebSignature object.
	    JsonWebSignature jws = new JsonWebSignature();
	    
	    // The payload of the JWS is JSON content of the JWT Claims
	    jws.setPayload(claims.toJson());

	    KeyStore keystore = KeyStore.getInstance("PKCS12");
	    keystore.load(this.getClass().getClassLoader().getResourceAsStream(tokenInfo.getKeyFileName()), tokenInfo.getKeyFilePassword().toCharArray());
	    PrivateKey key = (PrivateKey)keystore.getKey(tokenInfo.getKeyAlias(), tokenInfo.getKeyFilePassword().toCharArray());
	    
	    // The JWT is signed using the private key
	    jws.setKey(key);

	    // Set the Key ID (kid) header because it's just the polite thing to do.
	    // We only have one key in this example but a using a Key ID helps
	    // facilitate a smooth key rollover process
	    jws.setKeyIdHeaderValue(tokenInfo.getKeyId());

	    // Set the signature algorithm on the JWT/JWS that will integrity protect the claims
	    jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);

	    // Sign the JWS and produce the compact serialization or the complete JWT/JWS
	    // representation, which is a string consisting of three dot ('.') separated
	    // base64url-encoded parts in the form Header.Payload.Signature
	    // If you wanted to encrypt it, you can simply set this jwt as the payload
	    // of a JsonWebEncryption object and set the cty (Content Type) header to "jwt".
	    String jwt = jws.getCompactSerialization();
	    
	    // Now you can do something with the JWT. Like send it to some other party
	    // over the clouds and through the interwebs.
	    return jwt;
	}
}
